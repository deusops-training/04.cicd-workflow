FROM golang:1.20.3

WORKDIR /app
COPY . .
RUN ["go", "build"]
EXPOSE 8080

CMD [ "go", "run", "outyet" ]
